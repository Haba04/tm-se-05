package ru.habibrahmanov.tm;

import ru.habibrahmanov.tm.view.Bootstrap;
import ru.habibrahmanov.tm.view.command.help.HelpCommand;
import ru.habibrahmanov.tm.view.command.project.*;
import ru.habibrahmanov.tm.view.command.task.*;

public class Application {
    public static final Class[] commands = {
            HelpCommand.class,
            ProjectFindAllCommand.class, ProjectMergeCommand.class, ProjectPersistCommand.class, ProjectRemoveAllCommand.class,
            ProjectRemoveOneCommand.class, ProjectUpdateCommand.class,
            TaskFindAllCommand.class, TaskFindOneCommand.class, TaskMergeCommand.class, TaskPersistCommand.class,
            TaskRemoveOneCommand.class, TaskUpdateCommand.class
    };

    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(commands);
        bootstrap.start();
    }
}
