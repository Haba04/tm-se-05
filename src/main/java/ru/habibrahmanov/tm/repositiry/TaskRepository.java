package ru.habibrahmanov.tm.repositiry;

import ru.habibrahmanov.tm.entity.Task;

import java.util.*;
import java.util.stream.Collectors;

public class TaskRepository {
    private Map<String, Task> taskMap = new LinkedHashMap<>();

    public void persist(Task task) {
        taskMap.put(task.getId(), task);
    }

    public Task findOne(String taskId) {
        return taskMap.get(taskId);
    }

    public Map<String, Task> findAll() {
        return taskMap;
    }

    public void removeOne(String taskId) {
        taskMap.remove(taskId);
    }

    public boolean removeAll(String projectId) {
        Iterator<Map.Entry<String, Task>> iterator = taskMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Task> entryTask = iterator.next();
            if (entryTask.getValue().getProjectId().equals(projectId)) {
                iterator.remove();
            }
        }
        return false;
    }

    public void update(String taskId, String name) {
        taskMap.get(taskId).setName(name);
    }

    public void merge(Task task) {
        if (taskMap.containsKey(task.getId())) {
            update(task.getId(), task.getName());
        } else {
            persist(task);
        }
    }
}

