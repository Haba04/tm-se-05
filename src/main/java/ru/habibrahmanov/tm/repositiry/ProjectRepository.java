package ru.habibrahmanov.tm.repositiry;

import ru.habibrahmanov.tm.entity.Project;

import java.util.*;


public class ProjectRepository {
    private Map<String, Project> projectMap = new LinkedHashMap<>();

    public Map<String, Project> getProjectMap() {
        return projectMap;
    }

    public void persist(Project project) {
        projectMap.put(project.getId(), project);
    }

    public Map<String, Project> findAll() {
        return projectMap;
    }

    public Project findOne(String projectId) {
        return projectMap.get(projectId);
    }

    public void removeAll() {
        projectMap.clear();
    }

    public void removeOne(String projectId) {
        projectMap.remove(projectId);
    }

    // if there is a project, edit it, if not, create
    public void merge(Project project) {
        if (projectMap.containsKey(project.getId())) {
            update(project.getId(), project.getName());
        }
        persist(project);
    }

    public void update(String projectId, String name) {
        projectMap.get(projectId).setName(name);
    }
}

