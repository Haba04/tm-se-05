package ru.habibrahmanov.tm.view.command.task;

import ru.habibrahmanov.tm.view.command.AbstractCommand;

public class TaskPersistCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-persist";
    }

    @Override
    public String getDescription() {
        return "create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER PROJECT ID");
        String projectId = bootstrap.getScanner().nextLine();
        System.out.println("ENTER TASK NAME");
        String name = bootstrap.getScanner().nextLine();
        bootstrap.getTaskService().persist(projectId, name);
        System.out.println("TASK CREATE SUCCESSFULLY");
    }
}
