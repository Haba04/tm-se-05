package ru.habibrahmanov.tm.view.command.project;

import ru.habibrahmanov.tm.view.command.AbstractCommand;

public class ProjectRemoveAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-remove-all";
    }

    @Override
    public String getDescription() {
        return "remove all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CLEAR]");
        bootstrap.getProjectService().removeAll();
        System.out.println("DELETE ALL PROJECTS");
    }
}
