package ru.habibrahmanov.tm.view.command.task;

import ru.habibrahmanov.tm.view.command.AbstractCommand;

public class TaskFindOneCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-find";
    }

    @Override
    public String getDescription() {
        return "find task by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[Find task by id]");
        System.out.println("[Enter task by id]");
        String taskId = bootstrap.getScanner().nextLine();
        bootstrap.getTaskService().findOne(taskId);
        System.out.println("TASK ID: " + bootstrap.getTaskService().findAll().get(taskId).getId() + " / NAME: " + bootstrap.getTaskService().findAll().get(taskId).getName());
    }
}
