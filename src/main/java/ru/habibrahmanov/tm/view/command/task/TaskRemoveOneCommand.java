package ru.habibrahmanov.tm.view.command.task;

import ru.habibrahmanov.tm.view.command.AbstractCommand;

public class TaskRemoveOneCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER TASK ID:");
        String taskId = bootstrap.getScanner().nextLine();
        bootstrap.getTaskService().removeOne(taskId);
        System.out.println("TASK REMOVED SUCCESSFULLY");
    }
}
