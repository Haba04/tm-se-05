package ru.habibrahmanov.tm.view.command.task;

import ru.habibrahmanov.tm.view.command.AbstractCommand;

public class TaskFindAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-find-all";
    }

    @Override
    public String getDescription() {
        return "find all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW ALL TASKS]");
//        bootstrap.getTaskService().findAllService();
//        for (Map.Entry<String, Task> entry : taskService.findAllService().entrySet()) {
//            System.out.println("ID: " + entry.getKey() + " / NAME: " + entry.getValue().getName());
//        }
        bootstrap.getTaskService().findAll().forEach((k, v)-> System.out.println("TASK ID: " + k + " / NAME: " + v.getName()));
    }
}
