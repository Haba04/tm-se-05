package ru.habibrahmanov.tm.view.command.help;

import ru.habibrahmanov.tm.view.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() throws Exception {
        for (final AbstractCommand command : bootstrap.getCommands().values()) {
            System.out.printf("%-20s > %s \n", command.getName(), command.getDescription());
        }
    }
}
