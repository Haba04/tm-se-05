package ru.habibrahmanov.tm.view.command.project;

import ru.habibrahmanov.tm.view.command.AbstractCommand;

public class ProjectPersistCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-persist";
    }

    @Override
    public String getDescription() {
        return "create new project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        String name = bootstrap.getScanner().nextLine();
        bootstrap.getProjectService().persist(name);
        System.out.println("CREATE NEW PROJECT SUCCESSFULLY");
    }
}
