package ru.habibrahmanov.tm.view.command.project;

import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.view.command.AbstractCommand;

import java.util.Map;

public class ProjectFindAllCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-find-all";
    }

    @Override
    public String getDescription() {
        return "show all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL PROJECTS]");
        bootstrap.getProjectService().findAll();
        for (Map.Entry<String, Project> entry : bootstrap.getProjectService().getProjectRepository().findAll().entrySet()) {
            System.out.println("PROJECT ID: " + entry.getValue().getId() + " / NAME: " + entry.getValue().getName());
        }
    }
}
