package ru.habibrahmanov.tm.view.command;

import ru.habibrahmanov.tm.view.Bootstrap;

public abstract class AbstractCommand {
        protected Bootstrap bootstrap;
        public void setBootstrap(Bootstrap bootstrap) {
            this.bootstrap = bootstrap;
        }
        public abstract String getName();
        public abstract String getDescription();
        public abstract void execute() throws Exception;
}

