package ru.habibrahmanov.tm.view.command.project;

import ru.habibrahmanov.tm.view.command.AbstractCommand;

public class ProjectUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-update";
    }

    @Override
    public String getDescription() {
        return "update project by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER ID");
        String projectId = bootstrap.getScanner().nextLine();
        System.out.println("ENTER NAME TO CHANGE");
        String name = bootstrap.getScanner().nextLine();
        bootstrap.getProjectService().update(projectId, name);
        System.out.println("PROJECT EDIT SUCCESSFULLY");
    }
}
