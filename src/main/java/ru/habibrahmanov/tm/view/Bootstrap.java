package ru.habibrahmanov.tm.view;

import ru.habibrahmanov.tm.repositiry.ProjectRepository;
import ru.habibrahmanov.tm.repositiry.TaskRepository;
import ru.habibrahmanov.tm.service.ProjectService;
import ru.habibrahmanov.tm.service.TaskService;
import ru.habibrahmanov.tm.view.command.AbstractCommand;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public final class Bootstrap {
    private ProjectRepository projectRepository = new ProjectRepository();
    private TaskRepository taskRepository = new TaskRepository();
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskService taskService = new TaskService(taskRepository);
    private Scanner scanner = new Scanner(System.in);
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init(final Class... commandsClass) {
        try {
            if (commandsClass == null) return;
            for (Class clazz : commandsClass) {
                registry(clazz);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void registry(Class clazz) throws IllegalAccessException, InstantiationException {
        AbstractCommand abstractCommand = (AbstractCommand) clazz.newInstance();
        abstractCommand.setBootstrap(this);
        final String nameCommand = abstractCommand.getName();
        final String descriptionCommand = abstractCommand.getDescription();
        if (nameCommand == null || nameCommand.isEmpty()) return;
        if (descriptionCommand == null || descriptionCommand.isEmpty()) return;
        commands.put(nameCommand, abstractCommand);
    }

    public void start() throws Exception {
        System.out.println("***WELCOME TO TASK MANAGER***");
        System.out.println("Enter \"help\" to show all commands.");
        String command = "";
        while (!command.equals("exit")) {
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public Scanner getScanner() {
        return scanner;
    }



//    public void init() throws IncorrectValueException {
//        System.out.println("***WELCOME TO TASK MANAGER***");
//        System.out.println("Enter HELP for show all commands.");
//
//        do {
//            sc = scanner.nextLine();
//
//            switch (sc.toLowerCase()) {
//                case (TerminalCommands.PROJECTCREATE):
//                case (TerminalCommands.PCR):
//                    menu.commandCreatePtoject();
//                    break;
//
//                case (TerminalCommands.PROJECTLIST):
//                case (TerminalCommands.PL):
//                    menu.commandShowProject();
//                    break;
//
//                case (TerminalCommands.SHOWALLTASK):
//                case (TerminalCommands.SAT):
//                    menu.commandShowAllTask();
//                    break;
//
//                case (TerminalCommands.PROJECTREMOVE):
//                case (TerminalCommands.PR):
//                    menu.commandRemoveProject();
//                    break;
//
//                case (TerminalCommands.PROJECTEDIT):
//                case (TerminalCommands.PE):
//                    menu.commandEditProject();
//                    break;
//
//                case (TerminalCommands.PROJECLEAR):
//                case (TerminalCommands.PCL):
//                    menu.commandClearProject();
//                    break;
//
//                case (TerminalCommands.PROJECTMERGE):
//                case (TerminalCommands.PM):
//                    menu.commandMergeProject();
//                    break;
//
//                case (TerminalCommands.TASKCREATE):
//                case (TerminalCommands.TCR):
//                    menu.commandCreateTask();
//                    break;
//
//                case (TerminalCommands.TASKCLIST):
//                case (TerminalCommands.TL):
//                    menu.commandShowTask();
//                    break;
//
//                case (TerminalCommands.TASKCREMOVE):
//                case (TerminalCommands.TR):
//                    menu.commandRemoveTask();
//                    break;
//
//                case (TerminalCommands.TASKCEDIT):
//                case (TerminalCommands.TE):
//                    menu.commandEditTask();
//                    break;
//
//                case (TerminalCommands.TASKMERGE):
//                case (TerminalCommands.TM):
//                    menu.commandMergeTask();
//                    break;
//
//                case (TerminalCommands.HELP):
//                    menu.commandHelpProject();
//                    break;
//            }
//        } while (!sc.equals("exit"));
//    }


}
